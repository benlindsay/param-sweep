#!/bin/bash

#==============================================================================
#
#   File:     param-sweep.sh
#   Author:   Ben Lindsay
#   Repo:     bitbucket.org/benlindsay/param-sweep.git
#
#   Creates and submits jobs with multiple variables based on information in a
#   trials file.
#
#==============================================================================

function usage {
cat << EOF
Usage: param-sweep.sh [-t <TRIALS>] [-i <INPUT>] [-s <SUB>] [-n] [-m]

<TRIALS> is a file (trials.txt by default) with headers. First column is name
of trial. New direcories are created using these names, or existing directories
with these names are filled if -m is specified. The remaining columns have
headers of the form [sub|inp]:<VARIABLENAME> where the sub or inp tells the
script whether to look for <VARIABLENAME> in the input file or the submission
script. The variable values are the entries in each column. <INPUT> is the
input file (bcp.input by default) with variables matching <TRIALS> headers. It
is copied to the new directories as bcp.input (whether or not different <INPUT>
value is provided) and variables from <TRIALS> headers are replaced with
corresponding values. <SUB> is the submission script file (sub.sh by default),
also copied to the new directories. Submission scripts are then submitted
unless -n (for no submission) is used.
EOF

exit 1
}

# Initialize variables with default values
trials="trials.txt" # Default parameters file
inp="bcp.input"     # Default input file template
sub="sub.sh"        # Default submission script template
nosub=0             # Flag to be set to 1 if creating folders but not
                    #   submitting jobs
nomake=0            # Flag to be set to 1 if not creating folders, just filling
                    #   existing ones that match names in the trials file

# Get input arguments
while getopts ":t:i:s:nm" opt; do
  case $opt in
    t)
      trials=$OPTARG ;;
    i)
      inp="$OPTARG" ;;
    s)
      sub="$OPTARG" ;;
    n)
      nosub=1 ;;
    m)
      nomake=1 ;;
    \?)
      usage ;;
    :)
      echo "Option -$OPTARG requires an argument."
      usage ;;
  esac
done

if [ ! -f $trials ]; then
  echo "$trials is not a valid file"
  usage
elif [ ! -f $inp ]; then
  echo "$inp is not a valid file"
  usage
elif [ ! -f $sub ]; then echo "$sub is not a valid file"
  usage
fi

if [ "$(command -v sbatch)" != "" ]; then
  sub_prog="sbatch"
elif [ "$(command -v qsub)" != "" ]; then
  sub_prog="qsub"
else
  echo "Either sbatch or qsub must be available to submit jobs"
  exit 1
fi

echo "Trials file:        $trials"
echo "Input file:         $inp"
echo "Submission script:  $sub"

# Get number of trials by counting the number of lines in the trials file
# and subracting 1
n=$(( $(awk '{print $1}' $trials | wc -l) - 1 ))
echo "$n trials"

# Get number of variables to replace by counting the number of columns and
# subtracting 1
nvars=$(( $(awk 'FNR == 1 {print NF}' $trials) -1 ))
echo "$nvars vars"

# Loop over each row and create an associated trial
for ((row=2; row<=$n+1; row++)); do

  # Get the name of the trial from the first column of the trials file and
  # create a directory with that name or verify directory existence, depending
  # on whether or not -m flag was specified
  name=$(awk 'FNR == '$row' {print $1}' $trials)
  if [ $nomake == 0 ]; then
    if [ -d $name ]; then
      echo "$name is already a directory. Exiting for safety."
      exit 1
    fi
    mkdir $name
  else
    if [ ! -d $name ]; then
      echo "If -m is used, then $name directory must already exist."
    fi
  fi

  # Copy the input file and submission script templates into the new directory
  cp $inp $name/$inp
  cp $sub $name/$sub

  # Loop over each column and replace the variable with the associated value
  # in the correct file
  for ((col=2; col<=$nvars+1; col++)); do

    # Get the column header which should look something like sub:<VARNAME> or
    # inp:<VARNAME> (but s:<VARNAME> and i:<VARNAME> are also acceptable)
    colheader=$(awk 'FNR == 1 {print $'$col'}' $trials)

    # Split the column header into the pre-colon flag (sub/s/inp/i) and
    # variable name
    flag=${colheader%:*}
    var=${colheader#*:}

    # Determine the file in which to look for the variable to replace
    if [ "$flag" = "s" ] || [ "$flag" = "sub" ]; then
      targetfile=$sub
    elif [ "$flag" = "i" ] || [ "$flag" = "inp" ]; then
      targetfile=$inp
    else
      echo "The header in column $col of $trials needs 'sub:' or 'inp:'"
      echo "at the beginning to identify the file to edit"
      exit 1
    fi

    # Get the desired value and replace the variable in the target file
    val=$(awk 'FNR == '$row' {print $'$col'}' $trials)
    sed -i -e "s/$var/$val/g" $name/$targetfile

  done

  echo "Created trial $name"

  # Submit jobs unless -n was used
  if [ $nosub == 0 ]; then
    echo "Submitting trial $name:"
    if [ -d $name ]; then
      cd $name
      sleep 1
      $sub_prog $sub
      cd ..
    else
      echo "No directory named $name"
    fi
  fi

done

